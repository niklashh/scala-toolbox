# Scala-toolbox

[![pipeline status](https://gitlab.com/niklashh/scala-toolbox/badges/master/pipeline.svg)](https://gitlab.com/niklashh/scala-toolbox/-/commits/master)

This container can be used for running scala inside [toolbox](https://github.com/containers/toolbox).

```console
toolbox create --image registry.gitlab.com/niklashh/scala-toolbox:latest -i scala-toolbox
toolbox enter scala-toolbox
```

## Building manually

To build the toolbox container

```console
./build.sh
```

To start the toolbox

```console
toolbox create --image localhost/scala-toolbox -i scala-toolbox
toolbox enter scala-toolbox
```

To remove the toolbox

```console
toolbox rm -f scala-toolbox
```

## License

MIT
