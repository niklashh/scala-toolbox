FROM registry.gitlab.com/niklashh/debian-toolbox:latest

LABEL com.github.containers.toolbox="true" \
      usage="This image is meant to be used with the toolbox command" \
      maintainer="Niklas Halonen <niklas.2.halonen@aalto.fi>"

ARG SBT_VERSION=1.5.5

RUN apt-get update \
 && apt-get -y install default-jre gnupg \
 && sed -i -e 's/ ALL$/ NOPASSWD:ALL/' /etc/sudoers \
 && touch /etc/localtime \
 && echo VARIANT_ID=container >> /etc/os-release \
 && cd /root \
 && curl -fLo /usr/local/bin/cs https://git.io/coursier-cli-"$(uname | tr LD ld)" \
 && chmod +x /usr/local/bin/cs \
 && cs install scala3-compiler scala3-repl \
 && mv /root/.local/share/coursier/bin/* /usr/local/bin \
 && scala3-repl --version \
 && echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" > /etc/apt/sources.list.d/sbt.list \
 && echo "deb https://repo.scala-sbt.org/scalasbt/debian /" > /etc/apt/sources.list.d/sbt_old.list \
 && curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | apt-key add \
 && apt-get update \
 && apt-get -y install sbt \
 && apt-get clean \
 && sbt --version
